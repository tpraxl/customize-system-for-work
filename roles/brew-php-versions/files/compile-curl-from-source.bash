#!/usr/bin/env bash

# Provide parallel legacy curl version to support php 5 compilation without degrading the system tools.

# See https://github.com/phpbrew/phpbrew/issues/939#issuecomment-556974953

curl -o /opt/curl-7.67.0.tar.gz https://curl.haxx.se/download/curl-7.67.0.tar.gz
tar zxvf  /opt/curl-7.67.0.tar.gz -C /opt
chown -R root:root curl-7.67.0
ln -s /opt/curl-7.67.0 /opt/curl
(cd /opt/curl && sudo ./configure --with-ssl=/usr/local/openssl-1.0 --prefix=/usr/local/curl-with-openssl-1.0 && sudo make && sudo make install)
