#!/usr/bin/env bash

# Provide parallel legacy openssl version to support php 5 compilation without degrading the system tools.

# See https://github.com/phpbrew/phpbrew/issues/939#issuecomment-556974953

curl -o /opt/openssl-1.0.2t.tar.gz https://www.openssl.org/source/openssl-1.0.2t.tar.gz
tar zxvf /opt/openssl-1.0.2t.tar.gz -C /opt
ln -s /opt/openssl-1.0.2t /opt/openssl-1.0
(cd /opt/openssl-1.0 && sudo ./config shared --prefix=/usr/local/openssl-1.0 --openssldir=/usr/local/openssl-1.0 && sudo make && sudo make install)
