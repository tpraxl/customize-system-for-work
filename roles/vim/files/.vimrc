syntax enable

colorscheme desert

let mapleader = ','  "The default leader is \, but a comma is much better.
set number	     "Activate line numbers.

"------------Visuals---------------"
set t_CO=256	     "Use 256 colors. 

"------------Search----------------"

"highlight all search results
set hlsearch
"highlight while typing
set incsearch

"------------Split Management------"
set splitbelow
set splitright

nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-H> <C-W><C-H>
nmap <C-L> <C-W><C-L>

"------------Mappings--------------"

"Make it easy to edit the Vimrc file.
nmap <Leader>ev :tabedit $MYVIMRC<cr>





"------------Auto-Commands---------"

"Automatically source the Vimrc file on save.
augroup autosourcing
	autocmd!
	autocmd BufWritePost .vimrc source %
augroup END

